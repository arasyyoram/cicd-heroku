import { useState } from "react";
import "./App.css";

function App() {
  const [demo, setDemo] = useState(2);
  const [disable, setDisable] = useState(false);

  return (
    <div>
      {!!demo && <h2>{demo}</h2>}
      {!!demo && <button onClick={() => setDemo((prev) => prev - 1)}>-</button>}
      <button
        onClick={() => {
          setDemo((prev) => {
            if (prev >= 5) {
              return prev;
            }

            return prev + 1;
          });
          // if (demo === 5) setDisable(true);
        }}
        disabled={demo >= 5}
      >
        +
      </button>
    </div>
  );
}

export default App;
